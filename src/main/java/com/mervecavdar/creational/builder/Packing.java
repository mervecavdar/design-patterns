package com.mervecavdar.creational.builder;

public interface Packing {

    public String pack();

}
