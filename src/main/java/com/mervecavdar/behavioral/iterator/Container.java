package com.mervecavdar.behavioral.iterator;

public interface Container {

    public Iterator getIterator();

}
