package com.mervecavdar.structural.adapter;

public interface MediaPlayer {

    public void play(String audioType, String fileName);

}
