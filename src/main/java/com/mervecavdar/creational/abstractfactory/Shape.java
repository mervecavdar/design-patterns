package com.mervecavdar.creational.abstractfactory;

public interface Shape {

    void draw();

}
