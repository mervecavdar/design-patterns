package com.mervecavdar.behavioral.iterator;

public class NameRepository implements Container {

    private static final String[] NAMES = {"Robert", "John", "Julie", "Lora"};

    @Override
    public Iterator getIterator() {
        return new NameIterator();
    }

    private class NameIterator implements Iterator {
        int index;

        @Override
        public boolean hasNext() {
            return index < NAMES.length;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return NAMES[index++];
            }
            return null;
        }
    }

}
