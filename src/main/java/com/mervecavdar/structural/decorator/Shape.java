package com.mervecavdar.structural.decorator;

public interface Shape {

    void draw();

}
