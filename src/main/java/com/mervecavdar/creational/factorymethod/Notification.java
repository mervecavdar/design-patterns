package com.mervecavdar.creational.factorymethod;

public interface Notification {

    void notifyUser();

}
