package com.mervecavdar.structural.adapter;

/**
 * Allows for two incompatible classes to work together by wrapping an interface around one of the existing classes.
 */
public class Main {

    public static void main(String[] args) {
        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play("mp3", "xxx.mp3");
        audioPlayer.play("mp4", "xxx.mp4");
        audioPlayer.play("vlc", "xxx.vlc");
        audioPlayer.play("avi", "xxx.avi");
    }

}
