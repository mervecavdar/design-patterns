package com.mervecavdar.behavioral.observer;

/**
 * Is a publish/subscribe pattern which allows a number of observer objects to see an event.
 */
public class Main {

    public static void main(String[] args) {
        Subject subject = new Subject();

        new HexaObserver(subject);
        new OctalObserver(subject);
        new BinaryObserver(subject);

        System.out.println("First state change: 15");
        subject.setState(15);
        System.out.println("Second state change: 10");
        subject.setState(10);
    }

}
