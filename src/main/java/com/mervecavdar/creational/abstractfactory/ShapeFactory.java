package com.mervecavdar.creational.abstractfactory;

public class ShapeFactory extends AbstractFactory {

    @Override
    public Shape getShape(ShapeType shapeType) {
        if (shapeType.equals(ShapeType.SQUARE)) {
            return new Square();
        } else if (shapeType.equals(ShapeType.RECTANGLE)) {
            return new Rectangle();
        }
        return null;
    }

}
