package com.mervecavdar.structural.composite;

/**
 * Takes a group of objects into a single object.
 */
public class Main {

    public static void main(String[] args) {
        Employee ceo = new Employee("John", "Ceo", 30000);
        Employee headSales = new Employee("Robert", "Head Sales", 20000);
        Employee headMarketing = new Employee("Michael", "Head Marketing", 20000);
        Employee sales1 = new Employee("Richard", "Sales", 10000);
        Employee sales2 = new Employee("Rob", "Sales", 10000);
        Employee marketing1 = new Employee("Laura", "Marketing", 10000);
        Employee marketing2 = new Employee("Bob", "Marketing", 10000);

        ceo.add(headSales);
        ceo.add(headMarketing);

        headSales.add(sales1);
        headSales.add(sales2);

        headMarketing.add(marketing1);
        headMarketing.add(marketing2);

        System.out.println(ceo);
        for (Employee headEmployee : ceo.getSubOrdinates()) {
            System.out.println(headEmployee);
            for (Employee employee : headEmployee.getSubOrdinates()) {
                System.out.println(employee);
            }
        }
    }

}
