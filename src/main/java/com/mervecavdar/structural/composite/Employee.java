package com.mervecavdar.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Employee {

    private final String name;

    private final String dept;

    private final int salary;

    private final List<Employee> subOrdinates;

    public Employee(String name, String dept, int sal) {
        this.name = name;
        this.dept = dept;
        this.salary = sal;
        subOrdinates = new ArrayList<>();
    }

    public void add(Employee e) {
        subOrdinates.add(e);
    }

    public void remove(Employee e) {
        subOrdinates.remove(e);
    }

    public List<Employee> getSubOrdinates() {
        return subOrdinates;
    }

    public String toString() {
        return ("Employee: [ Name: " + name + ", Dept: " + dept + ", Salary: " + salary + " ]");
    }

}
