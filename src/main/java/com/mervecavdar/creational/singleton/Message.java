package com.mervecavdar.creational.singleton;

public class Message {

    private static Message instance;

    private Message() {

    }

    public static Message getInstance() {
        if (instance == null)
            instance = new Message();
        return instance;
    }

    public void print() {
        System.out.println("Singleton");
    }

}
