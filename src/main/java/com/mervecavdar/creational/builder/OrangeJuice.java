package com.mervecavdar.creational.builder;

public class OrangeJuice extends ColdDrink {

    @Override
    public float price() {
        return 35.0f;
    }

    @Override
    public String name() {
        return "Orange Juice";
    }

}
