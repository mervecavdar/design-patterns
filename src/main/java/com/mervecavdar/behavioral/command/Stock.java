package com.mervecavdar.behavioral.command;

public class Stock {

    private static final String NAME = "ABC";

    private static final int QUANTITY = 10;

    public void buy() {
        System.out.println("Stock [ Name: " + NAME + ", Quantity: " + QUANTITY + " ] bought");
    }

    public void sell() {
        System.out.println("Stock [ Name: " + NAME + ", Quantity: " + QUANTITY + " ] sold");
    }

}
