package com.mervecavdar.creational.factorymethod;

public class NotificationFactory {

    public Notification createNotification(NotificationChannel notificationChannel) {
        if (notificationChannel == null)
            return null;
        switch (notificationChannel) {
            case SMS:
                return new SMSNotification();
            case EMAIL:
                return new EmailNotification();
            case PUSH:
                return new PushNotification();
            default:
                return null;
        }
    }

}
