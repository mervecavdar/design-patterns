package com.mervecavdar.behavioral.interpreter;

public interface Expression {

    public boolean interpret(String context);

}
