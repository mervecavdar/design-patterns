package com.mervecavdar.behavioral.visitor;

/**
 * Separates an algorithm from an object structure by moving the hierarchy of methods into one object.
 */
public class Main {

    public static void main(String[] args) {
        ComputerPart computer = new Computer();
        computer.accept(new ComputerPartDisplayVisitor());
    }

}
