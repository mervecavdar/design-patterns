package com.mervecavdar.creational.builder;

public class VegetarianBurger extends Burger {

    @Override
    public float price() {
        return 40.0f;
    }

    @Override
    public String name() {
        return "Vegetarian Burger";
    }

}
