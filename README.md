# Design Patterns

## Creational Patterns
Abstract Factory Pattern,
Builder Pattern,
Factory Method Pattern,
Prototype Pattern,
Singleton Pattern.

## Structural Patterns
Adapter Pattern,
Bridge Pattern,
Composite Pattern,
Decorator Pattern,
Facade Pattern,
Flyweight Pattern,
Proxy Pattern.

## Behavioral Patterns
Chain of Responsibility Pattern,
Command Pattern,
Interpreter Pattern,
Iterator Pattern,
Mediator Pattern,
Memento Pattern,
Observer Pattern,
State Pattern,
Strategy Pattern,
Template Method Pattern,
Visitor Pattern.
