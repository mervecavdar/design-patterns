package com.mervecavdar.behavioral.command;

public interface Order {

    void execute();

}
