package com.mervecavdar.creational.abstractfactory;

public enum ShapeType {

    SQUARE, RECTANGLE

}
