package com.mervecavdar.creational.factorymethod;

/**
 * Creates objects without specifying the exact class to create.
 */
public class Main {

    public static void main(String[] args) {
        NotificationFactory notificationFactory = new NotificationFactory();
        Notification notification = notificationFactory.createNotification(NotificationChannel.SMS);
        notification.notifyUser();
    }

}
