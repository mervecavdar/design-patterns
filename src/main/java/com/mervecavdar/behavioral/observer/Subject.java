package com.mervecavdar.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private final List<Observer> observersList = new ArrayList<>();

    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        notifyAllObservers();
    }

    public void attach(Observer observer) {
        observersList.add(observer);
    }

    public void notifyAllObservers() {
        for (Observer observer : observersList) {
            observer.update();
        }
    }

}
