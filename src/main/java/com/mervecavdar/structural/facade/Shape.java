package com.mervecavdar.structural.facade;

public interface Shape {

    void draw();

}
