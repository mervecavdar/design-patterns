package com.mervecavdar.structural.proxy;

/**
 * Provides a placeholder interface to an underlying object to control access, reduce cost, or reduce complexity.
 */
public class Main {

    public static void main(String[] args) {
        Image image = new ProxyImage("test.jpg");
        image.display();
        System.out.println("");
        image.display();
    }

}
