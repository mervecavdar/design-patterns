package com.mervecavdar.creational.factorymethod;

public enum NotificationChannel {

    SMS, EMAIL, PUSH

}
