package com.mervecavdar.structural.proxy;

public interface Image {

    void display();

}
