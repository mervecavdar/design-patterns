package com.mervecavdar.creational.singleton;

/**
 * Ensures only one instance of an object is created.
 */
public class Main {

    public static void main(String[] args) {
        Message message = Message.getInstance();
        message.print();
    }

}
