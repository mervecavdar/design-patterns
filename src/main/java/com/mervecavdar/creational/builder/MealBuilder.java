package com.mervecavdar.creational.builder;

public class MealBuilder {

    public Meal prepareVegetarianMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegetarianBurger());
        meal.addItem(new Coke());
        return meal;
    }

    public Meal prepareNonVegetarianMeal() {
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new OrangeJuice());
        return meal;
    }

}
