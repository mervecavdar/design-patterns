package com.mervecavdar.structural.flyweight;

public interface Shape {

    void draw();

}
