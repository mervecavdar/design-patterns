package com.mervecavdar.behavioral.templatemethod;

/**
 * Defines the skeleton of an algorithm as an abstract class, allowing its sub-classes to provide concrete behavior.
 */
public class Main {

    public static void main(String[] args) {
        Game game = new Cricket();
        game.play();
        System.out.println();
        game = new Football();
        game.play();
    }

}
