package com.mervecavdar.behavioral.mediator;

/**
 * Allows loose coupling between classes by being the only class that has detailed knowledge of their methods.
 */
public class Main {

    public static void main(String[] args) {
        User robert = new User("Robert");
        User john = new User("John");

        robert.sendMessage("Hi! John!");
        john.sendMessage("Hello! Robert!");
    }

}
