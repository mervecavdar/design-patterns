package com.mervecavdar.creational.builder;

/**
 * Uses to create complex objects.
 */
public class Main {

    public static void main(String[] args) {
        MealBuilder mealBuilder = new MealBuilder();

        Meal vegetarianMeal = mealBuilder.prepareVegetarianMeal();
        System.out.println("Vegetarian Meal");
        vegetarianMeal.showItems();
        System.out.println("Total Cost: " + vegetarianMeal.getCost());

        Meal nonVegetarianMeal = mealBuilder.prepareNonVegetarianMeal();
        System.out.println("Non-Vegetarian Meal");
        nonVegetarianMeal.showItems();
        System.out.println("Total Cost: " + nonVegetarianMeal.getCost());
    }

}
