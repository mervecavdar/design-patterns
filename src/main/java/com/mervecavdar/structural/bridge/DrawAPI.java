package com.mervecavdar.structural.bridge;

public interface DrawAPI {

    public void drawCircle(int radius, int x, int y);

}
