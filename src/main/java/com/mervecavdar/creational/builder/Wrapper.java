package com.mervecavdar.creational.builder;

public class Wrapper implements Packing {

    @Override
    public String pack() {
        return "Wrapper";
    }

}
